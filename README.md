JW Player Media Source Module
====

Easily embed videos from JW Player into your Drupal site.
JW Platform Media Source is a contrib module for Drupal 10
that allows you to connect your Drupal media to JW Player v2 APIs.

## Installation

1. Install the module: `composer require drupal/jw_player_media_source`
2. Enable the module in the Extend section of your Drupal admin dashboard.
3. Configure and enable: `/admin/config/media/jw-player-media-source`

## Usage

After installing JW Platform Media Source, you will need to configure it.

1. Go to the JW Platform Media Source configuration page.
2. Enter your JW Player API v2 key.
3. Enter the ID of the JW Player site that you want to use.
4. Click the "Save" button.

Once you have configured JW Platform Media Source,
you can start embedding videos from JW Player into your Drupal site.
To embed a video from JW Player, you can use the jwplayer custom field or block.

## Configuration

Configure JW Player players
Embed videos from JW Player
Manage JW Player videos

# This module also provides a number of additional features, such as:

1. WYSIWYG CKEditor5 Plugin to embed videos from JW Player
2. Custom field for embedding videos from JW Player
3. Block for embedding videos from JW Player
4. List of JW Player videos in the Content/Media section

## Maintainers

- Alberto Cocchiara (bigbabert)

## License

This project is licensed under the GPL-2.0-or-later.
