<?php

namespace Drupal\jw_player_media_source\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Controller routines for AJAX example routes.
 */
class JwPlayerController extends ControllerBase {

  /**
   * Retrieves the container.
   *
   * @return mixed
   *   The container.
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Retrieves the form builder.
   *
   * @return object
   *   The form builder.
   */
  public static function getFormInterface() {
    return static::getContainer()
      ->get('form_builder');
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'jw_player_media_source';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jw_player_media_source.settings'];
  }

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function jwMediaPage() {
    $config = $this->config('jw_player_media_source.settings');
    $jwPlayerKey = $config->get('js_ms_ids');
    $jwPlayerSecret = $config->get('jw_ms_secret');
    $jwPlayerApiUrl = $config->get('jw_ms_enable');
    if (!$jwPlayerKey || !$jwPlayerSecret || !$jwPlayerApiUrl) {
      $jw_settings_url = Url::fromUri('internal:/admin/config/media/jw-player-media-source');

      return [
        '#type' => 'markup',
        '#markup' => Markup::create('<p>' . $this->t('Please fill all the required fields in the JW Settings from at the page: @link.',
          ['@link' => Markup::create(Link::fromTextAndUrl($this->t('JW Media Player Settings'), $jw_settings_url)->toString())]
        ) . '</p>'),
      ];
    }
    $jwMediaFormView['title'] = [
      '#type' => 'item',
      '#markup' => '<h1>' . $this->t('JW Player Media Source') . '</h1>',
    ];
    $jwMediaFormView['description'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $this->t('This JW Media resource video list page') . '</p>',
    ];
    $jwMediaFormView["table"] = $this->formBuilder()->getForm('Drupal\jw_player_media_source\Form\MediaViewForm');
    return $jwMediaFormView;
  }

  /**
   * Callback for opening the modal form.
   */
  public function jwOpenModalForm() {
    $response = new AjaxResponse();

    // Get the modal form using the form builder.
    $modal_form = $this->getFormInterface()->getForm('Drupal\jw_player_media_source\Form\MediaViewForm');
    // Add an AJAX command to open a modal dialog with the form as the content.
    $response->addCommand(new OpenModalDialogCommand($this->t('JW Player Form'), $modal_form, ['width' => '1024'], NULL, '#jw_select_modal'));

    return $response;
  }

}
