<?php

namespace Drupal\jw_player_media_source\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'jw_video_script_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "jw_video_script_formatter",
 *   label = @Translation("JW Video Script Formatter"),
 *   module = "jw_player_media_source",
 *   field_types = {
 *     "field_jw_video_item"
 *   }
 * )
 */
class JwVideoScriptFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      // Set the value of the script tag.
      $elements[$delta] = [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#attributes' => [
          'src' => $item->value,
        ],
      ];
    }
    return $elements;
  }

}
