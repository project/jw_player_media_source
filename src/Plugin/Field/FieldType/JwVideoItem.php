<?php

namespace Drupal\jw_player_media_source\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_jw_video_item' field type.
 *
 * @FieldType(
 *   id = "field_jw_video_item",
 *   label = @Translation("JW Video"),
 *   module = "jw_player_media_source",
 *   description = @Translation("JW Video field type."),
 *   default_widget = "jw_video_widget",
 *   default_formatter = "jw_video_script_formatter"
 * )
 */
class JwVideoItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 256,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel('JW Video url')
      ->setRequired(TRUE);
    return $properties;
  }

}
