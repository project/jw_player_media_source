<?php

namespace Drupal\jw_player_media_source\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'field_jw_video_item' widget.
 *
 * @FieldWidget(
 *   id = "jw_video_widget",
 *   module = "jw_player_media_source",
 *   label = @Translation("JW Video Widget"),
 *   field_types = {
 *     "field_jw_video_item"
 *   }
 * )
 */
class JwVideoWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $value = $items[$delta]->value ?? '';
    $element += [
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['row'],
      ],
      'video' => [
        '#type' => 'submit',
        '#value' => $this->t('Select JW Video'),
        '#ajax' => [
          'callback' => [static::class, 'jwOpenModalForm'],
          'url' => Url::fromRoute('entity.jwmedia.modal'),
          'options' => [
            'query' => [
              FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
            ],
          ],
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Loading JW Media modal...'),
          ],
        ],
        '#attached' => [
          'library' => [
            'jw_player_media_source/jwblock',
          ],
        ],
      ],
      'value' => [
        '#type' => 'textfield',
        '#default_value' => $value,
        '#maxlength' => 256,
        '#size' => 64,
        '#placeholder' => $this->t('Select a JW Video'),
        '#description' => $this->t('This is a JW Video field that embeds a video from JW Player.'),
        '#prefix'      => "<div id='jw_player_embed_field' class='col-sm-12'>",
        '#suffix'      => '</div>',
        '#attributes' => [
          'class' => ['jw-video', 'disabled'],
          'readonly' => 'readonly',
        ],
        '#element_validate' => [
          [$this, 'validate'],
        ],
      ],
    ];

    return $element;
  }

  /**
   * Validate the video script field.
   */
  public function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    $value = trim($value);
    if (empty($value)) {
      $form_state->setError($element, $this->t('Please select a JW Video.'));
      return;
    }
    if (!empty($value) && !preg_match('/^https:\/\/cdn.jwplayer.com\/players\/[a-zA-Z0-9]{8}-[a-zA-Z0-9]{8}.js/', $value)) {
      $form_state->setError($element, $this->t('Please select a valid JW Video.'));
      return;
    }

    $form_state->setValueForElement($element, $value);
  }

  /**
   * Callback for opening the modal form.
   */
  public function jwOpenModalForm($form, FormStateInterface $form_state) {
    $content = $this->getFormInterface()->getForm('Drupal\jw_player_media_source\Form\MediaViewForm');
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($this->t('JW Player Form'), $content, ['width' => '1024']));
    return $response;
  }

  /**
   * Retrieves the container.
   *
   * @return mixed
   *   The container.
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Retrieves the form builder.
   *
   * @return object
   *   The form builder.
   */
  public static function getFormInterface() {
    return static::getContainer()
      ->get('form_builder');
  }

}
