<?php

namespace Drupal\jw_player_media_source\Plugin\Menu;

use Drupal\Core\Menu\LocalTaskDefault;

/**
 * Provides a 'JW Menu Link' menu link.
 */
class JwMenuLinkTask extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function setActive($active = TRUE) {
    $config = $this->config();
    $jwEnabled = $config->get('jw_ms_enable');
    if (!$jwEnabled) {
      return FALSE;
    }
    else {
      $this->active = $active;
      return $this;
    }
  }

  /**
   * Retrieves the container.
   *
   * @return mixed
   *   The container.
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Retrieves the configuration settings.
   *
   * @return object
   *   The configuration settings.
   */
  public static function config() {
    return static::getContainer()
      ->get('config.factory')
      ->get('jw_player_media_source.settings');
  }

}
