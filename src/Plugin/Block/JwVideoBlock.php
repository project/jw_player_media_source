<?php

namespace Drupal\jw_player_media_source\Plugin\Block;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Provides a 'JW Video' Block.
 *
 * @Block(
 *   id = "jw_video_block",
 *   admin_label = @Translation("JW Video block"),
 *   category = @Translation("JW Player Media Source"),
 * )
 */
class JwVideoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['jw_video'])) {
      $script = Markup::create('<script src="' . $config['jw_video'] . '"></script>');
    }
    else {
      return [];
    }
    $jw_item['video'] = [
      '#type' => 'item',
      '#markup' => $script,
    ];
    return $jw_item;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'jw_video' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'jw_player_media_source/jwblock';

    $form['embed_video'] = [
      '#type' => 'submit',
      '#value' => $this->t('Select JW Video'),
      '#ajax' => [
        'callback' => [static::class, 'jwOpenModalForm'],
        'url' => Url::fromRoute('entity.jwmedia.modal'),
        'options' => [
          'query' => [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading JW Media modal...'),
        ],
      ],
    ];

    $form['jw_video'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JW Video'),
      '#description' => $this->t('This is a JW Video block that embeds a video from JW Player.'),
      '#default_value' => $this->configuration['jw_video'],
      '#prefix'      => "<div id='jw_player_embed' class='col-sm-12'>",
      '#suffix'      => '</div>',
      '#attributes' => [
        'class' => ['jw-video', 'disabled'],
        'readonly' => 'readonly',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['jw_video'] = $values['jw_video'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if ($form_state->getValue('jw_video') === 'John') {
      $form_state->setErrorByName('jw_video', $this->t('You can not say hello to John.'));
    }
  }

  /**
   * Callback for opening the modal form.
   */
  public function jwOpenModalForm($form, FormStateInterface $form_state) {
    $content = $this->getFormInterface()->getForm('Drupal\jw_player_media_source\Form\MediaViewForm');
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($this->t('JW Player Form'), $content, ['width' => '1024']));
    return $response;
  }

  /**
   * Retrieves the container.
   *
   * @return mixed
   *   The container.
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Retrieves the form builder.
   *
   * @return object
   *   The form builder.
   */
  public static function getFormInterface() {
    return static::getContainer()
      ->get('form_builder');
  }

}
