<?php

namespace Drupal\jw_player_media_source\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\RestripeCommand;
use Drupal\Core\Ajax\ScrollTopCommand;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\Markup;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class StatsSettings.
 *
 * @package Drupal\jw_player_media_source\Form
 */
class MediaViewForm extends FormBase {
  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jw_player_media_source_settings';
  }

  /**
   * Get Editable config names.
   *
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['jw_player_media_source.settings'];
  }

  /**
   * Disable caching for ajax problem with form.
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Retrieves the container.
   *
   * @return mixed
   *   The container.
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Retrieves the form builder.
   *
   * @return object
   *   The form builder.
   */
  public static function getFormRequest() {
    return static::getContainer()
      ->get('request_stack')->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Attach the library for pop-up dialogs/modals.
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'jw_player_media_source/admin.jwbox';
    $form['#prefix'] = '<div id="jw_player_form_wrapper">';
    $form['#suffix'] = '</div>';

    $results = $form_state->getValue('results') ? $form_state->getValue('results') : $this->jwMediaApiCall($form_state->getValue('page_index') ? $form_state->getValue('page_index') : 1, $form_state->getValue('search') ? $form_state->getValue('search') : NULL);
    $values = $form_state->getValues();
    if ($this->jwPlayerFormIsDialog()) {
      $tableHeaders = [
        "id" => ["data" => $this->t('ID'), 'field' => 'id'],
        "title" => ["data" => $this->t('Title'), 'field' => 'title'],
        "duration" => ["data" => $this->t('Duration'), 'field' => 'duration'],
        "thumb" => ["data" => $this->t('Thumbnail'), 'field' => 'thumb'],
        "status" => ["data" => $this->t('Status'), 'field' => 'status'],
      ];
    }
    else {
      $tableHeaders = [
        "id" => ["data" => $this->t('ID'), 'field' => 'id'],
        "title" => ["data" => $this->t('Title'), 'field' => 'title'],
        "duration" => ["data" => $this->t('Duration'), 'field' => 'duration'],
        // "embed" => ["data" => $this->t('Embed code'), 'field' => 'embed'],
        "thumb" => ["data" => $this->t('Thumbnail'), 'field' => 'thumb'],
        "status" => ["data" => $this->t('Status'), 'field' => 'status'],
      ];
    }
    $form['jw_player_media_source'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['container'],
        'style' => 'padding: 0;',
      ],
    ];

    $form['jw_player_media_source']["search_fieldset"] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search JW Media by Title'),
      '#attributes' => [
        'class' => ['container-inline', 'inline', 'jw-video-search-wrapper'],
      ],
    ];
    $form['jw_player_media_source']["search_fieldset"]["search"] = [
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => isset($results['rows'][0]['title']) && !empty($results['rows'][0]['title']) ? $results['rows'][0]['title'] : $this->t('Search by Title'),
        'class' => ['jw-video-search', 'inline'],
        'id' => 'jw-video-search',
      ],
      '#default_value' => $form_state->getValue('search') ? $form_state->getValue('search') : '',
    ];
    $form['jw_player_media_source']["search_fieldset"]['submit_search'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#name' => 'button-search',
      '#attributes' => [
        'class' => ['button', 'button--primary', 'inline'],
        'style' => 'margin-left: 10px;',
      ],
      '#ajax' => [
        'callback' => [$this, 'pagerCallback'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading JW Media search results ...'),
        ],
        'wrapper' => 'jw_player_form_wrapper',
      ],
      '#submit' => ['::searchCallback'],

    ];
    $form['jw_player_media_source']["search_fieldset"]['reset_search'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#name' => 'button-reset',
      '#attributes' => [
        'class' => ['button', 'button--secondary', 'inline'],
      ],
      '#ajax' => [
        'callback' => [$this, 'pagerCallback'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading JW Media results ...'),
        ],
        'wrapper' => 'jw_player_form_wrapper',
      ],
      '#submit' => ['::resetFilterCallback'],
      '#states' => [
        'visible' => [
          ':input[name="search"]' => ['filled' => TRUE],
        ],
      ],
    ];

    // Add pagination data.
    $form['jw_player_media_source']['page_index'] = [
      '#type' => 'hidden',
      '#value' => $form_state->getValue('page_index') ? $form_state->getValue('page_index') : 1,
      '#attributes' => [
        'id' => 'jw_player_page_index',
      ],
    ];
    $form['jw_player_media_source']['page_count'] = [
      '#type' => 'hidden',
      '#value' => $form_state->getValue('results') && isset($form_state->getValue('results')['pager']['total_pages']) ? $form_state->getValue('results')['pager']['total_pages'] : $results['pager']['total_pages'],
      '#attributes' => [
        'id' => 'jw_player_page_count',
      ],
    ];

    // Add table.
    $form['jw_player_media_source']['table'] = [
      '#type' => 'tableselect',
      '#empty' => $this->t('No JW Videos yet.'),
      '#header' => $tableHeaders,
      '#options' => $results['rows'],
      "#multiple" => FALSE,
      '#weight' => 10,
    ];

    // Add pagination buttons.
    $form['jw_player_media_source']['pager'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['pager'],
      ],
      '#weight' => 40,
    ];

    $form['jw_player_media_source']['pager']['actions']['prev'] = [
      '#type' => 'submit',
      '#value' => $this->t('Previous'),
      '#attributes' => [
        'class' => ['button', 'disabled', 'prev-button', 'button--secondary'],
        'disabled' => 'disabled',
      ],
      '#name' => 'button-prev',
      '#submit' => ['::prevPage'],
      '#executes_submit_callback' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'pagerCallback'],
        'event' => 'click',
        'method' => 'replace',
        'progress' => [
          'type' => 'throbber',
          'message' => Markup::create('<span class="floatright">' . $this->t('Loading JW Media prev page @page', ['@page' => $values && isset($values['page_index']) ? $values['page_index'] - 1 . ' of ' . ($values && isset($values['page_count']) ? $values['page_count'] : $results['pager']['total_pages']) : 1 . ' of ' . ($values && isset($values['page_count']) ? $values['page_count'] : $results['pager']['total_pages'])]) . '</span>'),
        ],
        'wrapper' => 'jw_player_form_wrapper',
      ],
    ];
    $form['jw_player_media_source']['pager']['actions']['next'] = [
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#attributes' => [
        'class' => ['button', 'next-button'],
      ],
      '#name' => 'button-next',
      '#submit' => ['::nextPage'],
      '#executes_submit_callback' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'pagerCallback'],
        'event' => 'click',
        'method' => 'replace',
        'progress' => [
          'type' => 'throbber',
          'message' => Markup::create('<span class="floatright">' . $this->t('Loading JW Media next page @page', ['@page' => $values && isset($values['page_index']) ? $values['page_index'] + 1 . ' of ' . ($values && isset($values['page_count']) ? $values['page_count'] : $results['pager']['total_pages']) : 2 . ' of ' . ($values && isset($values['page_count']) ? $values['page_count'] : $results['pager']['total_pages'])]) . '</span>'),
        ],
        'wrapper' => 'jw_player_form_wrapper',
      ],
    ];

    // Add a submit button that handles the embed of the media.
    $form['jw_player_media_source']['pager']['actions']['embed'] = [
      '#type' => 'submit',
      '#value' => $this->t('Embed'),
      '#name' => 'button-embed',
      '#attributes' => [
        'class' => ['button--primary', 'button', 'embed-button', 'floatright'],
      ],
      '#ajax' => [
        'callback' => [$this, 'embedCallback'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => Markup::create('<span class="floatright">' . $this->t('Loading JW Media embed script ...') . '</span>'),
        ],
        'wrapper' => 'embedcode',
      ],
      '#submit' => ['::submitForm'],
      '#prefix' => '<span class="col-4 right">',
      '#suffix' => '</span>',
    ];
    $form['jw_player_media_source']['pager']['actions']['embedcode'] = [
      '#type' => 'processed_text',
      '#text' => $form_state->getValue('embedcode_text') ? $form_state->getValue('embedcode_text') : '',
      '#prefix' => '<span class="floatright" id="embedcode">',
      '#suffix' => '</span>',
      '#format' => 'plain_text',
    ];

    return $form;
  }

  /**
   * Api call to retrieve JW Media with paginatination.
   */
  public function jwMediaApiCall($page, $q = NULL) {

    $config = $this->config('jw_player_media_source.settings');
    $logger = $this->getLogger('jw_player_media_source');
    $jwPlayerSourceEnabled = $config->get('jw_ms_enable') ? $config->get('jw_ms_enable') : FALSE;
    $jwPlayerSourceIDs = $config->get('js_ms_ids') ? explode(",", $config->get('js_ms_ids')) : FALSE;
    $jwPlayerSecret = $config->get('jw_ms_secret') ? $config->get('jw_ms_secret') : FALSE;

    if (!$jwPlayerSourceEnabled || !$jwPlayerSourceIDs || !$jwPlayerSecret) {
      return [];
    }
    $client = new Client();
    $all_rows = [];
    foreach ($jwPlayerSourceIDs as $id) {
      try {
        if ($q) {
          $endpoint = 'https://api.jwplayer.com/v2/sites/' . $id . '/media/?q=title:' . $q . '&page=' . $page . '&page_length=10&sort=created%3Adsc';
        }
        else {
          $endpoint = 'https://api.jwplayer.com/v2/sites/' . $id . '/media/?page=' . $page . '&page_length=10&sort=created%3Adsc';
        }
        $request = $client->request(
          "GET",
          $endpoint,
          [
            'timeout' => 5,
            'headers' => [
              'Accept' => 'application/json',
              'Content-Type' => 'application/json',
              'Authorization' => 'Bearer ' . $jwPlayerSecret,
              'verify' => FALSE,
            ],
          ]
        );
        if ($request->getStatusCode() == 200) {
          $res = $request ? $request->getBody() : '{}';
          $data = $res->getContents();
          $decode = Json::decode($data);
          foreach ($decode['media'] as $key => $value) {
            $resMedia = '{}';
            $mediaData = '{}';
            $decodeMedia = [];
            if (isset($value['id']) && isset($value['media_type'])) {
              try {
                $getMediaUrl = 'https://cdn.jwplayer.com/v2/media/' . $value['id'];
                $requestMedia = $client->request(
                  "GET",
                  $getMediaUrl,
                  [
                    'timeout' => 5,
                    'headers' => [
                      'Accept' => 'application/json',
                      'Content-Type' => 'application/json',
                      'Authorization' => 'Bearer ' . $jwPlayerSecret,
                      'verify' => FALSE,
                    ],
                  ]
                );
                if ($requestMedia->getStatusCode() == 200) {
                  $resMedia = $requestMedia ? $requestMedia->getBody() : '{}';
                  $mediaData = $resMedia->getContents();
                  $decodeMedia = Json::decode($mediaData);

                  if (isset($decodeMedia['playlist'][0]["images"]) && is_array($decodeMedia['playlist'][0]["images"]) && count($decodeMedia['playlist'][0]["images"]) > 0) {
                    $decode['media'][$key]['metadata']["thumb"] = $decodeMedia['playlist'][0]["images"][0];
                  }
                  else {
                    $decode['media'][$key]['metadata']["thumb"] = "";
                  }
                }
                else {
                  $decode['media'][$key]['metadata']["thumb"] = "";
                }
              }
              catch (RequestException $e) {
                $decode['media'][$key]['metadata']["thumb"] = "";
                $logger->debug('JW Player - Media response: @error', ['@error' => json_encode($e->getMessage())]);
              }
            }
            else {
              unset($decode['media'][$key]);
            }
          }
          array_push($all_rows, $decode);
        }
      }
      catch (RequestException $e) {
        $logger->debug('JW Player - Media response: @error', ['@error' => $e->getMessage()]);
      }
    }
    $rows = [];

    foreach ($all_rows as $key => $value) {
      if (isset($all_rows[$key]['media'])) {
        foreach ($all_rows[$key]['media'] as $id => $media) {

          $duration = $media['duration'] ? DrupalDateTime::createFromTimestamp(floor($media['duration']), new \DateTimeZone('UTC'), ['langcode' => 'en']) : DrupalDateTime::createFromTimestamp(floor(563.093994140625), new \DateTimeZone('UTC'), ['langcode' => 'en']);
          if ($this->jwPlayerFormIsDialog()) {
            $row = [
              'id' => Markup::create('<span>' . $media['id'] . '</span>'),
              'title' => Markup::create('<span>' . $media['metadata']['title'] . '</span>'),
              'duration' => Markup::create('<span>' . $duration->format('H:i:s') . '</span>'),
              'thumb' => isset($media['metadata']['thumb']['src']) ? Markup::create('<img src="' . $media['metadata']['thumb']['src'] . '" width="100px" height="100px">') : '',
              'status' => Markup::create('<span>' . $media['status'] . '</span>'),
            ];
          }
          else {
            $row = [
              'id' => Markup::create('<span>' . $media['id'] . '</span>'),
              'title' => Markup::create('<span>' . $media['metadata']['title'] . '</span>'),
              'duration' => Markup::create('<span>' . $duration->format('H:i:s') . '</span>'),
              'thumb' => isset($media['metadata']['thumb']['src']) ? Markup::create('<img src="' . $media['metadata']['thumb']['src'] . '" width="100px" height="100px">') : '',
              'status' => Markup::create('<span>' . $media['status'] . '</span>'),
            ];
          }
          $rows[$id] = $row;
        }
      }
    }

    $total_pges = $all_rows[0]['total'] ?? 0;
    $page_length = $all_rows[0]['page_length'] ?? 10;

    $results = [
      'pager' => [
        'current_page' => !empty($all_rows) && isset($all_rows[0]['page']) ? $all_rows[0]['page'] : [],
        'total_pages' => floor($total_pges / $page_length),
        'total_items' => $total_pges,
      ],
      'rows' => $rows,
    ];

    return $results;
  }

  /**
   * Callback for opening the modal form.
   */
  public function jwPlayerFormIsDialog() {
    $currentRequest = $this->getFormRequest();
    $wrapper_format = $currentRequest->query
      ->get(MainContentViewSubscriber::WRAPPER_FORMAT);
    return (in_array($wrapper_format, [
      'drupal_ajax',
      'drupal_modal',
      'drupal_dialog',
      'drupal_dialog.off_canvas',
    ])) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Return $form['jw_player_media_source'];.
  }

  /**
   * Callback to embed the JW Video Player script.
   */
  public function embedCallback(array &$form, FormStateInterface $form_state) {
    // Find out what was submitted.
    $config = $this->config('jw_player_media_source.settings');
    $logger = $this->getLogger('jw_player_media_source');
    $jwPlayerId = $config->get('jw_ms_player_id') ? $config->get('jw_ms_player_id') : "-";
    $values = $form_state->getValues();
    $results = $form_state->getValue('results') ? $form_state->getValue('results') : $this->jwMediaApiCall($form_state->getValue('page_index') ? $form_state->getValue('page_index') : 1);

    if (isset($results['rows'][$values['table']])) {
      $logger->debug('JW Player - $value[$key] : @$values', ['@$values' => $results[$values['table']]]);
      $mediaID = str_replace('<span>', '', str_replace('</span>', '', $results['rows'][$values['table']]['id']));
    }
    $logger->debug('JW Player - $values: @$values', ['@$values' => json_encode($values)]);
    $script = $mediaID && !empty($mediaID) ? 'https://cdn.jwplayer.com/players/' . $mediaID . '-' . $jwPlayerId . '.js' : $this->t('Please select a media to embed');
    $form_state->setValue('embedcode_text', $script);

    $form['jw_player_media_source']['pager']['actions']['embedcode']['#text'] = $script;
    $form_state->setRebuild();
    $response = new AjaxResponse();

    if ($this->jwPlayerFormIsDialog()) {
      $response->addCommand(new InvokeCommand(NULL, 'embedCallback', [$script]));
      $response->addCommand(new CloseModalDialogCommand(FALSE, '#jw_select_modal'));
    }
    else {
      $response->addCommand(new ReplaceCommand('#embedcode ', $form['jw_player_media_source']['pager']['actions']['embedcode']));
    }
    $messages = ['#type' => 'status_messages'];
    $response->addCommand(new PrependCommand('#jw_player_form_wrapper', $messages));
    $form_state->setResponse($response);

    return $response;
  }

  /**
   * Callback for reset filters.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function resetFilterCallback(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['search']) {
      $form['jw_player_media_source']['table']['#options'] = [];
      $form['jw_player_media_source']["search_fieldset"]["search"]['#value'] = "";
      $form_state->setValue('search', '');
      $form_state->setRebuild();
      $results = $this->jwMediaApiCall(1);
    }
    else {
      return $form;
    }
    $page = $results['pager']['current_page'];
    $form['jw_player_media_source']['table']['#options'] = $results['rows'];
    $form['jw_player_media_source']['page_index']['#value'] = (int) $results['pager']['current_page'];
    $form['jw_player_media_source']['page_count']['#value'] = $results['pager']['total_pages'];
    $form['jw_player_media_source']['pager']['actions']['next']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media next page @page',
      [
        '@page' => (
        $results['pager']['current_page'] + 1) .
        ' of ' .
        $results['pager']['total_pages'],
      ]
        ) .
        '</span>'
      );
    $form['jw_player_media_source']['pager']['actions']['prev']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t('Loading JW Media prev page @page',
      [
        '@page' => (
        $results['pager']['current_page'] - 1) .
        ' of ' .
        $results['pager']['total_pages'],
      ]
        ) .
        '</span>'
      );
    if ($page > 1) {
      $form['jw_player_media_source']['pager']['actions']['prev']['#disabled'] = FALSE;
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['class'] = [
        'button',
        'prev-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['disabled'] = FALSE;
    }
    if ($page >= $results['pager']['total_pages']) {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = TRUE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = [
        'button',
        'disabled',
        'next-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = 'disabled';
    }
    else {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = FALSE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = ['button', 'next-button'];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = FALSE;
    }
    $form_state->setRebuild();
    // Update table rows and pager.
    $form_state->setValue('results', $results);
    $form_state->setValue('page_index', $results['pager']['current_page']);
    $form_state->setValue('page_count', $results['pager']['total_pages']);
    $form_state->setValue('search', '');
    $form_state->setRebuild();

    return $form;
  }

  /**
   * Callback for search by title.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function searchCallback(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['search']) {
      $form['jw_player_media_source']['table']['#options'] = [];
      $form['jw_player_media_source']["search_fieldset"]["search"]['#value'] = $values['search'];
      $form_state->setRebuild();
      $results = $this->jwMediaApiCall(1, $values['search']);
      // print_r('<pre>');
      // var_dump($results['rows']);die;.
    }
    else {
      return $form;
    }
    $page = $results['pager']['current_page'];
    $form['jw_player_media_source']['table']['#options'] = $results['rows'];
    $form['jw_player_media_source']['page_index']['#value'] = (int) $results['pager']['current_page'];
    $form['jw_player_media_source']['page_count']['#value'] = $results['pager']['total_pages'];
    $form['jw_player_media_source']['pager']['actions']['next']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media next page @page',
        [
          '@page' => ($results['pager']['current_page'] + 1) .
          ' of ' .
          $results['pager']['total_pages'],
        ]
          ) .
          '</span>'
        );
    $form['jw_player_media_source']['pager']['actions']['prev']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media prev page @page',
        [
          '@page' => ($results['pager']['current_page'] - 1) .
          ' of ' .
          $results['pager']['total_pages'],
        ]
          ) .
          '</span>'
        );
    if ($page > 1) {
      $form['jw_player_media_source']['pager']['actions']['prev']['#disabled'] = FALSE;
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['class'] = [
        'button',
        'prev-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['disabled'] = FALSE;
    }
    if ($page >= $results['pager']['total_pages']) {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = TRUE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = [
        'button',
        'disabled',
        'next-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = 'disabled';
    }
    else {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = FALSE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = ['button', 'next-button'];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = FALSE;
    }
    $form_state->setRebuild();
    // Update table rows and pager.
    $form_state->setValue('results', $results);
    $form_state->setValue('page_index', $results['pager']['current_page']);
    $form_state->setValue('page_count', $results['pager']['total_pages']);
    $form_state->setValue('search', $values['search']);
    $form_state->setRebuild();

    return $form;
  }

  /**
   * Callback for both pager ajax buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function pagerCallback(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['search'] && !empty($values['search'])) {
      $form['jw_player_media_source']["search_fieldset"]["search"]['#value'] = $values['search'];
    }
    else {
      $form['jw_player_media_source']["search_fieldset"]["search"]['#value'] = '';
    }
    $form['jw_player_media_source']['table']['#options'] = $values['results']['rows'];
    $form['jw_player_media_source']['page_index']['#value'] = $values['results']['pager']['current_page'];
    $form['jw_player_media_source']['page_count']['#value'] = $values['results']['pager']['total_pages'];
    $form['jw_player_media_source']['pager']['actions']['next']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media next page @page',
        [
          '@page' => ($values['page_index'] + 1) .
          ' of ' .
          $values['page_count'],
        ]
          ) .
          '</span>'
        );
    $form['jw_player_media_source']['pager']['actions']['prev']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media prev page @page',
        [
          '@page' => ($values['page_index'] - 1) .
          ' of ' .
          $values['page_count'],
        ]
          ) . '</span>'
        );
    if ($values['page_index'] > 1) {
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['class'] = [
        'button',
        'prev-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['disabled'] = FALSE;
    }
    else {
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['class'] = [
        'button',
        'disabled',
        'prev-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['disabled'] = TRUE;
    }
    if ($values['page_index'] >= $values['results']['pager']['total_pages']) {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = TRUE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = [
        'button',
        'disabled',
        'next-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = 'disabled';
    }
    else {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = FALSE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = ['button', 'next-button'];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = FALSE;
    }
    $form_state->setRebuild();
    $response = new AjaxResponse();
    $response->addCommand(new InvokeCommand(NULL, 'pagerCallback', [$values['page_index']]));
    $response->addCommand(new ScrollTopCommand('#jw_player_form_wrapper'));
    $response->addCommand(new ReplaceCommand('#jw_player_form_wrapper table', $form['jw_player_media_source']['table']));
    $response->addCommand(new ReplaceCommand('#jw_player_form_wrapper #jw_player_page_index', $form['jw_player_media_source']['page_index']));
    $response->addCommand(new ReplaceCommand('#jw_player_form_wrapper #jw_player_page_count', $form['jw_player_media_source']['page_count']));
    $response->addCommand(new ReplaceCommand('#jw_player_form_wrapper .jw-video-search-wrapper', $form['jw_player_media_source']["search_fieldset"]));
    $response->addCommand(new ReplaceCommand('#jw_player_form_wrapper .next-button', $form['jw_player_media_source']['pager']['actions']['next']));
    $response->addCommand(new ReplaceCommand('#jw_player_form_wrapper .prev-button', $form['jw_player_media_source']['pager']['actions']['prev']));

    $response->addCommand(new RestripeCommand('#jw_player_form_wrapper'));

    $messages = ['#type' => 'status_messages'];
    $response->addCommand(new PrependCommand('#jw_player_form_wrapper', $messages));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function nextPage(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (isset($form['jw_player_media_source']['page_index']['#value'])) {
      $page = (int) $form['jw_player_media_source']['page_index']['#value'] + 1;
    }
    else {
      $page = $values['page_index'] ? (int) $values['page_index'] + 1 : 2;
    }
    if ($values['search']) {
      $results = $this->jwMediaApiCall($page, $values['search']);
      $form_state->setValue('search', $values['search']);
    }
    else {
      $results = $this->jwMediaApiCall($page);
    }
    $page = $results['pager']['current_page'];
    // Update table rows and pager.
    $form['jw_player_media_source']['table']['#options'] = $results['rows'];
    $form['jw_player_media_source']['page_index']['#value'] = (int) $page;
    $form['jw_player_media_source']['page_count']['#value'] = $results['pager']['total_pages'];
    $form['jw_player_media_source']['pager']['actions']['next']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media next page @page',
        [
          '@page' => $results['pager']['current_page'] + 1 .
          ' of ' .
          $results['pager']['total_pages'],
        ]
          ) .
          '</span>'
        );
    $form['jw_player_media_source']['pager']['actions']['prev']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media prev page @page',
        [
          '@page' => $results['pager']['current_page'] - 1 .
          ' of ' .
          $results['pager']['total_pages'],
        ]
          ) .
          '</span>'
        );
    if ($page > 1) {
      $form['jw_player_media_source']['pager']['actions']['prev']['#disabled'] = FALSE;
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['class'] = [
        'button',
        'prev-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['disabled'] = FALSE;
    }
    if ($page >= $results['pager']['total_pages']) {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = TRUE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = [
        'button',
        'disabled',
        'next-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = 'disabled';
    }
    else {
      $form['jw_player_media_source']['pager']['actions']['next']['#disabled'] = FALSE;
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['class'] = ['button', 'next-button'];
      $form['jw_player_media_source']['pager']['actions']['next']['#attributes']['disabled'] = FALSE;
    }
    $form_state->setValue('results', $results);
    $form_state->setValue('page_index', $page);
    $form_state->setRebuild();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prevPage(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (isset($form['jw_player_media_source']['page_index']['#value'])) {
      $page = (int) $form['jw_player_media_source']['page_index']['#value'] - 1;
    }
    else {
      $page = $values['page_index'] ? (int) $values['page_index'] - 1 : 1;
    }

    if ($values['search']) {
      $results = $this->jwMediaApiCall($page, $values['search']);
      $form_state->setValue('search', $values['search']);
    }
    else {
      $results = $this->jwMediaApiCall($page);
    }
    $page = $results['pager']['current_page'];
    // Update table rows and pager.
    $form['jw_player_media_source']['table']['#options'] = $results['rows'];
    $form['jw_player_media_source']['page_index']['#value'] = (int) $page;
    $form['jw_player_media_source']['page_count']['#value'] = $results['pager']['total_pages'];
    $form['jw_player_media_source']['pager']['actions']['next']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media next page @page',
        [
          '@page' => $results['pager']['current_page'] + 1 .
          ' of ' .
          $results['pager']['total_pages'],
        ]
          ) .
          '</span>'
        );
    $form['jw_player_media_source']['pager']['actions']['prev']['#ajax']['progress']['message'] = Markup::create(
      '<span class="floatright">' .
      $this->t(
        'Loading JW Media prev page @page',
        [
          '@page' => $results['pager']['current_page'] - 1 .
          ' of ' .
          $results['pager']['total_pages'],
        ]
          ) .
          '</span>'
        );
    if ($page < 1) {
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['class'] = [
        'button',
        'disabled',
        'prev-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['disabled'] = 'disabled';
    }
    else {
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['class'] = [
        'button',
        'prev-button',
        'button--secondary',
      ];
      $form['jw_player_media_source']['pager']['actions']['prev']['#attributes']['disabled'] = FALSE;
    }

    $form_state->setValue('results', $results);
    $form_state->setValue('page_index', $page);
    $form_state->setRebuild();
    return $form;
  }

}
