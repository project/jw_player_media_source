<?php

namespace Drupal\jw_player_media_source\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Configure custom_rest settings for this site.
 */
class JWPlayerMediaSourceSettings extends ConfigFormBase {
  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jw_player_media_source_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jw_player_media_source.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('jw_player_media_source.settings');

    $form['jw_ms_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable JW Player Media source'),
      '#description' => $this->t('If enabled JW Player Media source will be enabled for videos media type.'),
      '#default_value' => $config->get('jw_ms_enable'),
    ];
    $form['settings_jw_player_media_source'] = [
      '#type'  => 'details',
      '#title' => $this->t('JW Player Media Source Settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="jw_ms_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['settings_jw_player_media_source']['jw_ms_secret'] = [
      '#type' => 'textarea',
      '#rows' => 1,
      '#title' => $this->t('JW Player API V2 Secret'),
      '#attributes' => ['placeholder' => $this->t('XXXXXXXXXX_XXXXXXXX_XXXXXX_XXXXX'), 'data-disable-refocus' => 'true'],
      '#description' => $this->t('Please enter the secret that you can founf at bottom of the page in JW Player dashboard -> Account -> API Credentials'),
      '#default_value' => $config->get('jw_ms_secret'),
      '#required'      => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="jw_ms_enable"]' => ['checked' => TRUE],
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'playerListCallback'],
        'event' => 'change',
        'wrapper' => 'player_list',
      ],
    ];

    $form['settings_jw_player_media_source']['js_ms_ids'] = [
      '#type' => 'textarea',
      '#rows' => 1,
      '#title' => $this->t('JW Player Media Source IDs'),
      '#attributes' => ['placeholder' => $this->t('XXXX, YYYyY, zZZzZZ, ...'), 'data-disable-refocus' => 'true'],
      '#description' => $this->t('Please enter the JW Player Media Source IDs separated by comma'),
      '#default_value' => $config->get('js_ms_ids'),
      '#required'      => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="jw_ms_enable"]' => ['checked' => TRUE],
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'playerListCallback'],
        'event' => 'change',
        'wrapper' => 'player_list',
      ],
    ];
    if ($config->get('jw_ms_secret') && $config->get('js_ms_ids')) {
      $players_list = $this->jwPlayerApiCall($config->get('js_ms_ids'), $config->get('jw_ms_secret'));
      $form_state->setValue('jw_players', $players_list);
      // var_dump($config->get('jw_ms_player_id'));
      // die;.
    }
    else {
      $players_list = $form_state->getValue('jw_players') ? $form_state->getValue('jw_players') : ["-" => "No Players"];
    }

    $form['settings_jw_player_media_source']['jw_ms_player_id'] = [
      '#title' => $this->t('JW Player'),
      '#type' => 'select',
      '#description'   => $this->t('Select JW Player Player'),
      '#options' => $players_list,
      '#default_value' => $config->get('jw_ms_player_id') && array_key_exists($config->get('jw_ms_player_id'), $players_list) ? $config->get('jw_ms_player_id') : array_key_first($players_list),
      '#required'      => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="jw_ms_enable"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div id="player_list">',
      '#suffix' => '</div>',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('jw_player_media_source.settings');
    $config->set('jw_ms_secret', $form_state->getValue('jw_ms_secret'));
    $config->set('jw_ms_enable', $form_state->getValue('jw_ms_enable'));
    $config->set('js_ms_ids', $form_state->getValue('js_ms_ids'));
    $config->set('jw_ms_player_id', $form_state->getValue('jw_ms_player_id'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Callback for opening the modal form.
   */
  public function playerListCallback(array &$form, FormStateInterface $form_state) {
    $jwIDs = $form_state->getValue('js_ms_ids');
    $jwSecret = $form_state->getValue('jw_ms_secret');
    $logger = $this->getLogger('jw_player_media_source');
    if (!$jwIDs || !$jwSecret) {
      $form['settings_jw_player_media_source']['jw_ms_player_id']['#options'];
    }
    else {
      $players_list = $this->jwPlayerApiCall($jwIDs, $jwSecret);
      $form_state->setValue('jw_players', $players_list);
      $form['settings_jw_player_media_source']['jw_ms_player_id']['#options'] = $players_list;
      $form_state->setRebuild();
    }
    $response = new AjaxResponse();

    $response->addCommand(new ReplaceCommand('#player_list ', $form['settings_jw_player_media_source']['jw_ms_player_id']));

    $messages = ['#type' => 'status_messages'];
    $response->addCommand(new PrependCommand('#jw_player_form_wrapper', $messages));
    $form_state->setResponse($response);

    return $response;
  }

  /**
   * Api call to retrieve JW Media with paginatination.
   */
  public function jwPlayerApiCall($jwIDs, $jwSecret) {

    $jwIDs = explode(",", $jwIDs);
    $client = new Client();
    $players_list = ["-" => "No Players"];
    $logger = $this->getLogger('jw_player_media_source');

    if (!$jwIDs || !$jwSecret || !$client || !is_array($jwIDs)) {
      return $players_list;
    }

    foreach ($jwIDs as $id) {
      if (preg_match('/^[a-zA-Z0-9]{8}.js/', $id)) {
        return $players_list;
      }
      try {
        $endpoint = 'https://api.jwplayer.com/v2/sites/' . $id . '/players';
        $request = $client->request(
          "GET",
          $endpoint,
          [
            'timeout' => 5,
            'headers' => [
              'Accept' => 'application/json',
              'Content-Type' => 'application/json',
              'Authorization' => 'Bearer ' . $jwSecret,
              'verify' => FALSE,
            ],
          ]
        );
        if ($request->getStatusCode() == 200) {
          $res = $request ? $request->getBody() : '{}';
          $data = $res->getContents();
          $decode = Json::decode($data);
          foreach ($decode['players'] as $key => $value) {
            $players_list[$value['id']] = $value['metadata']['name'];
          }
        }
      }
      catch (RequestException $e) {
        $logger->debug('JW Player - Debug: $decode - @error', ['@error' => $e->getMessage()]);
      }
    }
    return $players_list;
  }

}
