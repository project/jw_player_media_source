<?php

namespace Drupal\Tests\jw_player_media_source\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the JW Playe rMedia Source module.
 *
 * @group jw_player_media_source
 */
class JwPlayerMediaSourceTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['jw_player_media_source'];
  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A simple user.
   *
   * @var \Drupal\user\UserInterface
   */
  private $user;

  /**
   * Perform initial setup tasks that run before every test method.
   */
  public function setUp(): void {
    parent::setUp();
    $this->user = $this->DrupalCreateUser([
      'administer site configuration',
      'administer jw player media source',
    ]);
  }

  /**
   * Tests that the Lorem ipsum page can be reached.
   */
  public function testJwMediaPageExists() {
    // Login.
    $this->drupalLogin($this->user);

    // Generator test:
    $this->drupalGet('admin/config/media/jw-player-media-source');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests the config form.
   */
  public function testConfigForm() {
    // Login.
    $this->drupalLogin($this->user);

    // Access config page.
    $this->drupalGet('admin/config/media/jw-player-media-source');
    $this->assertSession()->statusCodeEquals(200);
    // Test the form elements exist and have defaults.
    $config = \Drupal::config('jw_player_media_source.settings');
    $this->assertSession()->fieldValueEquals(
      'jw_ms_enable',
      FALSE
    );
  }

}
