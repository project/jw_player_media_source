(function jwPlayerEmbed($) {
  // Argument passed from InvokeCommand.
  $.fn.embedCallback = function jwPlayerEmbedScript(argument) {
    if ($('input[name="settings[jw_video]"]').length > 0) {
      $('input[name="settings[jw_video]"]').attr('value', argument);
      $('input[name="settings[jw_video]"]').attr('default_value', argument);
    }
    if ($('#jw_player_embed_field input').length > 0) {
      $('#jw_player_embed_field input').attr('value', argument);
      $('#jw_player_embed_field input').attr('default_value', argument);
    }
  };
  // Argument passed from InvokeCommand.
  $.fn.pagerCallback = function pagination(argument) {
    return argument;
  };
})(jQuery);
